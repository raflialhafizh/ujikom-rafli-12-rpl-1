DROP TABLE detail_pinjam;

CREATE TABLE `detail_pinjam` (
  `id_detail_pinjam` int(11) NOT NULL AUTO_INCREMENT,
  `id_inventaris` int(11) NOT NULL,
  `jumlahh` int(11) NOT NULL,
  `id_peminjaman` int(11) NOT NULL,
  `status` enum('dipinjam','dikembalikan') NOT NULL,
  PRIMARY KEY (`id_detail_pinjam`),
  KEY `id_inventaris` (`id_inventaris`),
  KEY `index` (`id_peminjaman`),
  CONSTRAINT `detail_pinjam_ibfk_1` FOREIGN KEY (`id_inventaris`) REFERENCES `invantaris` (`id_inventaris`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `detail_pinjam_ibfk_2` FOREIGN KEY (`id_peminjaman`) REFERENCES `peminjaman` (`id_peminjaman`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

INSERT INTO detail_pinjam VALUES("11","2","30","27","dikembalikan");
INSERT INTO detail_pinjam VALUES("12","2","-10","28","dikembalikan");
INSERT INTO detail_pinjam VALUES("13","2","1","29","dikembalikan");
INSERT INTO detail_pinjam VALUES("14","2","10","30","dikembalikan");
INSERT INTO detail_pinjam VALUES("15","2","10","30","dikembalikan");
INSERT INTO detail_pinjam VALUES("16","2","10","31","dikembalikan");
INSERT INTO detail_pinjam VALUES("17","2","40","32","dikembalikan");
INSERT INTO detail_pinjam VALUES("18","1","20","34","dikembalikan");
INSERT INTO detail_pinjam VALUES("19","2","0","40","dikembalikan");
INSERT INTO detail_pinjam VALUES("20","2","0","40","dikembalikan");
INSERT INTO detail_pinjam VALUES("21","2","0","40","dikembalikan");
INSERT INTO detail_pinjam VALUES("22","2","0","40","dikembalikan");
INSERT INTO detail_pinjam VALUES("23","2","0","40","dikembalikan");
INSERT INTO detail_pinjam VALUES("24","2","3","40","dikembalikan");
INSERT INTO detail_pinjam VALUES("25","2","20","41","dikembalikan");
INSERT INTO detail_pinjam VALUES("26","2","20","41","dikembalikan");
INSERT INTO detail_pinjam VALUES("27","1","1","42","dipinjam");
INSERT INTO detail_pinjam VALUES("28","2","20","43","dikembalikan");
INSERT INTO detail_pinjam VALUES("29","2","20","44","dipinjam");
INSERT INTO detail_pinjam VALUES("30","3","-50","45","dipinjam");
INSERT INTO detail_pinjam VALUES("31","3","20","46","dikembalikan");
INSERT INTO detail_pinjam VALUES("32","3","50","47","dikembalikan");
INSERT INTO detail_pinjam VALUES("33","3","70","48","dikembalikan");
INSERT INTO detail_pinjam VALUES("34","3","10","50","dikembalikan");



DROP TABLE invantaris;

CREATE TABLE `invantaris` (
  `id_inventaris` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `kondisi` text NOT NULL,
  `spesifikasi` text NOT NULL,
  `ket` text NOT NULL,
  `jumlah` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `tgl_register` date NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `kode_inventaris` varchar(11) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  `sumber` text NOT NULL,
  PRIMARY KEY (`id_inventaris`),
  KEY `ig_petugas` (`id_petugas`),
  KEY `id_ruang` (`id_ruang`),
  KEY `id_jenis` (`id_jenis`),
  KEY `id_jenis_2` (`id_jenis`),
  KEY `id_jenis_3` (`id_jenis`),
  KEY `id_petugas` (`id_petugas`),
  KEY `id_petugas_2` (`id_petugas`),
  CONSTRAINT `invantaris_ibfk_6` FOREIGN KEY (`id_jenis`) REFERENCES `jenis` (`id_jenis`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `invantaris_ibfk_7` FOREIGN KEY (`id_ruang`) REFERENCES `ruang` (`id_ruang`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `invantaris_ibfk_8` FOREIGN KEY (`id_petugas`) REFERENCES `petugas` (`id_petugas`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO invantaris VALUES("1","Proyektor","baik","BanQ 1080p","LAB RPL 1","34","3","2019-04-02","9","BRG0001","5","sekolah");
INSERT INTO invantaris VALUES("2","Meja","Baru","Almunium","LAB RPL 1","20","3","2019-04-05","9","BRG0002","5","Dinas");
INSERT INTO invantaris VALUES("3","Kursi","Baik","baik","Keperluan kelas","100","3","2019-04-06","9","BRG0003","5","sekolah");
INSERT INTO invantaris VALUES("4","Laptop","Baik","Lenovo ideapad 100","LAB RPL 1","10","3","2019-04-06","9","BRG0004","5","Sekolah");



DROP TABLE jenis;

CREATE TABLE `jenis` (
  `id_jenis` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenis` varchar(50) NOT NULL,
  `kode_jenis` varchar(50) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO jenis VALUES("3","Elektronik","JNS0001","baru ");
INSERT INTO jenis VALUES("4","Alat Olahraga","JNS0002","baru");



DROP TABLE level;

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(50) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO level VALUES("1","admin");
INSERT INTO level VALUES("2","operator");
INSERT INTO level VALUES("3","peminjam");



DROP TABLE pegawai;

CREATE TABLE `pegawai` (
  `id_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pegawai` varchar(30) NOT NULL,
  `nip` int(11) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `no_telfon` int(13) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(15) NOT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO pegawai VALUES("2","nola","3236652","bogor","213546545","namaku","nama");
INSERT INTO pegawai VALUES("3","bagas","20031322","Bogor","210002542","nameo","nemo");



DROP TABLE peminjaman;

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT,
  `tgl_pinjam` date NOT NULL,
  `tgl_kembali` date NOT NULL,
  `status_peminjaman` enum('Dipinjam','Dikembalikan') NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  PRIMARY KEY (`id_peminjaman`),
  KEY `id_pegawai` (`id_pegawai`),
  CONSTRAINT `peminjaman_ibfk_1` FOREIGN KEY (`id_pegawai`) REFERENCES `pegawai` (`id_pegawai`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;

INSERT INTO peminjaman VALUES("27","2019-04-04","2019-04-06","Dikembalikan","2");
INSERT INTO peminjaman VALUES("28","2019-04-05","2019-04-09","Dikembalikan","2");
INSERT INTO peminjaman VALUES("29","2019-04-05","2019-04-09","Dikembalikan","2");
INSERT INTO peminjaman VALUES("30","0000-00-00","2019-04-13","Dikembalikan","2");
INSERT INTO peminjaman VALUES("31","2019-04-05","2019-04-06","Dikembalikan","2");
INSERT INTO peminjaman VALUES("32","2019-04-05","2019-04-07","Dikembalikan","2");
INSERT INTO peminjaman VALUES("33","2019-04-05","2019-04-07","Dipinjam","2");
INSERT INTO peminjaman VALUES("34","2019-04-05","2019-04-12","Dikembalikan","2");
INSERT INTO peminjaman VALUES("35","0000-00-00","2019-04-07","Dipinjam","2");
INSERT INTO peminjaman VALUES("36","0000-00-00","2019-04-07","Dipinjam","2");
INSERT INTO peminjaman VALUES("37","0000-00-00","2019-04-05","Dipinjam","2");
INSERT INTO peminjaman VALUES("38","0000-00-00","2019-04-05","Dipinjam","2");
INSERT INTO peminjaman VALUES("39","2019-04-05","2019-04-05","Dipinjam","2");
INSERT INTO peminjaman VALUES("40","2019-04-05","2019-04-05","Dikembalikan","2");
INSERT INTO peminjaman VALUES("41","2019-04-05","2019-04-05","Dikembalikan","2");
INSERT INTO peminjaman VALUES("42","2019-04-05","2019-04-05","Dipinjam","3");
INSERT INTO peminjaman VALUES("43","2019-04-05","2019-04-06","Dikembalikan","3");
INSERT INTO peminjaman VALUES("44","2019-04-05","2019-04-13","Dipinjam","2");
INSERT INTO peminjaman VALUES("45","2019-04-06","2019-04-06","Dipinjam","3");
INSERT INTO peminjaman VALUES("46","2019-04-06","2019-04-06","Dikembalikan","3");
INSERT INTO peminjaman VALUES("47","2019-04-06","2019-04-06","Dikembalikan","3");
INSERT INTO peminjaman VALUES("48","2019-04-06","2019-04-08","Dikembalikan","2");
INSERT INTO peminjaman VALUES("49","2019-04-06","2019-04-02","Dipinjam","3");
INSERT INTO peminjaman VALUES("50","2019-04-07","2019-04-07","Dikembalikan","3");



DROP TABLE petugas;

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(50) NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT 'N' COMMENT 'N Tidak Aktif, Y Aktif',
  `nama_petugas` varchar(50) NOT NULL,
  `id_level` int(11) NOT NULL,
  PRIMARY KEY (`id_petugas`),
  KEY `id_level` (`id_level`),
  CONSTRAINT `petugas_ibfk_1` FOREIGN KEY (`id_level`) REFERENCES `level` (`id_level`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

INSERT INTO petugas VALUES("5","admin","hafizhr890@gmail.com","admin","Y","dodo","1");
INSERT INTO petugas VALUES("6","panto","bhstory.tv@gmail.com","operator","Y","kaldu","2");



DROP TABLE ruang;

CREATE TABLE `ruang` (
  `id_ruang` int(11) NOT NULL AUTO_INCREMENT,
  `nama_ruang` varchar(50) NOT NULL,
  `kode_ruang` varchar(50) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id_ruang`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

INSERT INTO ruang VALUES("9","LAB RPL 1","RNG0001","baik");



