<?php
include "head.php"; 
?>
<!DOCTYPE html><html>

	<div class="content-page">
		<div class="content">
			<div class="">
				<div class="page-header-title">
				</div>
			</div>
			<div class="page-content-wrapper ">
				<div class="container">		
					<div class="row">
						<div class="col-md-12">
							<div class="table-responsive">
							<div class="panel panel-primary">
								<div class="panel-body">
									<h4 class="m-b-30 m-t-0">Data Pegawai</h4>
									<table id="datatable-buttons" class="table table-striped table-bordered">
										
									<thead>
									<tr>
										<th>No.</th>
										<th>Nama Pegawai</th>
										<th>NIP</th>
										<th>Alamat</th>
										<th>No telfon</th>
										<th>username</th>
										<th>password</th>
                    <th>Opsi</th>
									</tr>
									</thead>
									<tbody>
		 <?php
                    include "konek.php";
                    $query_mysql = mysqli_query ($konek, "SELECT * FROM pegawai ORDER BY id_pegawai") or die (mysqli_error());
                    $i = 1;
                    while($data = mysqli_fetch_array($query_mysql)){
						?>
                                <tr>
                                    <td class="text-center"><?php echo $i++;?></td>
                                    <td class="text-center"><?php echo $data['nama_pegawai']; ?></td>
                                    <td class="text-center"><?php echo $data['nip']; ?></td>
									<td class="text-center"><?php echo $data['alamat']; ?></td>
									<td class="text-center"><?php echo $data['no_telfon']; ?></td>
									<td class="text-center"><?php echo $data['username']; ?></td>
									<td class="text-center"><?php echo $data['password']; ?></td>
									<td class="text-center">
                                   <a href="#" data-toggle="modal" data-target="#modal-edit<?php echo $data['id_pegawai'];?>" class="btn btn-success">Edit</a>
                                     </td>
								
			<div class="modal fade" id="modal-edit<?php echo $data['id_pegawai'];?>">
			  <div class="modal-dialog">
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Data Pegawai</h4>
					  <div class="modal-body">
						<form method="POST" action="u_pegawai.php">
						  <div class="box-body">
							<input type="hidden" name="id_pegawai" value="<?php echo $data['id_pegawai'] ?>" required>
							<div class="form-group">
							  <label>Nama Pegawai</label>
							  <input name="nama_pegawai" type="text" class="form-control" placeholder="" onkeypress="hurufangka(event)" value="<?php echo $data['nama_pegawai'] ?>" required>
							</div>
							<div class="form-group">
							  <label>NIP</label>
							  <input name="nip" type="number" class="form-control" placeholder="" onkeypress="hurufangka(event)" value="<?php echo $data['nip'] ?>" required>
							</div>
							<div class="form-group">
							  <label>Alamat</label>
							  <input name="alamat" type="text" class="form-control" placeholder="" onkeypress="hurufangka(event)" value="<?php echo $data['alamat'] ?>" required>
							</div>
							<div class="form-group">
							  <label>No Telfon</label>
							  <input name="no_telfon" type="number" class="form-control" placeholder="" onkeypress="hurufangka(event)" value="<?php echo $data['no_telfon'] ?>" required>
							</div>
							<div class="form-group">
							  <label>Username</label>
							  <input name="username" type="text" class="form-control" placeholder="" onkeypress="hurufangka(event)" value="<?php echo $data['username'] ?>" required>
							</div>
							<div class="form-group">
							  <label>Password</label>
							  <input name="password" type="text" class="form-control" placeholder="" onkeypress="hurufangka(event)" value="<?php echo $data['password'] ?>" required>
							</div>
							<div class="modal-footer">
							<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
							<button type="submit" class="btn btn-primary" value="update">Edit</button>
						  </div>
						</form>
					  </div>
				    </div>
				  </div>
				</div>
					<?php } ?>
								
                </tbody>
              </table>
                                    <div class="row">
											<div class="col-xs- col-sm- m-t-10">
												<div class=" text-center">
													<button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#myModal">+ Tambah Data</button>
												</div>
												<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
													<div class="modal-dialog">
														<div class="modal-content">
															<div class="modal-header">
																<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
																<h4 class="modal-title" id="myModalLabel">Input Data Pegawai</h4>
															</div>
															<div class="modal-body">
                                                            <div class="col-md-12">
                                                            <div class="row">
						<div class="col-sm-12">
							<div class="panel panel-primary">
								<div class="panel-body">
									<form action="proses_simpan_pegawai.php" method="post" class="form-horizontal" role="form">
										<div class="form-group">
											<label class="col-md-2 control-label">Nama pegawai</label>
											<div class="col-md-10">
												<input type="text" class="form-control" name="nama_pegawai" placeholder="Nama pegawai" onkeypress="hurufangka(event)" required></div>
                                        </div>
                                        <div class="form-group">
											<label class="col-md-2 control-label">NIP</label>
											<div class="col-md-10">
												<input type="number" class="form-control" name="nip" placeholder="NIP" onkeypress="hurufangka(event)" required></div>
											</div>
										<div class="form-group">
											<label class="col-md-2 control-label">Alamat</label>
											<div class="col-md-10">
												<input type="text" class="form-control" name="alamat" placeholder="Alamat" onkeypress="hurufangka(event)" required></div>
										</div>
										<div class="form-group">
											<label class="col-md-2 control-label">No Telfon</label>
											<div class="col-md-10">
												<input type="number" class="form-control" name="no_telfon" placeholder="No telfon" onkeypress="hurufangka(event)" required></div>
										</div>
										<div class="form-group">
											<label class="col-md-2 control-label">Username</label>
											<div class="col-md-10">
												<input type="text" class="form-control" name="username" placeholder="Username" onkeypress="hurufangka(event)" required></div>
										</div>
										<div class="form-group">
											<label class="col-md-2 control-label">Password</label>
											<div class="col-md-10">
												<input type="text" class="form-control" name="password" placeholder="Password" onkeypress="hurufangka(event)" required></div>
										</div>
										<div class="form-group m-b-0">
											<div class="col-sm-offset-8 col-sm-9">
												<button type="submit" name="send" class="btn btn-info waves-effect waves-light">Simpan</button>
											</div>
                                        </div>
									</form>
								</div>
							</div>
						</div>
					</div>
					
                                                        </div>
															<div class="modal-footer">
														
															</div>
														</div>
													</div>
												</div>
											</div>
										
								</div>
							</div>
						</div>
					</div>
				</div>
            </div>
            
		</div>
		<footer class="footer">© 2016 WebAdmin - All Rights Reserved.</footer>
	</div>
</div>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/modernizr.min.js"></script>
<script src="assets/js/detect.js"></script>
<script src="assets/js/fastclick.js"></script>
<script src="assets/js/jquery.slimscroll.js"></script>
<script src="assets/js/jquery.blockUI.js"></script>
<script src="assets/js/waves.js"></script>
<script src="assets/js/wow.min.js"></script>
<script src="assets/js/jquery.nicescroll.js"></script>
<script src="assets/js/jquery.scrollTo.min.js"></script>
<script type="text/javascript" src="assets/js/input_detect.js"></script>
<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="assets/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="assets/plugins/datatables/buttons.bootstrap.min.js"></script>
<script src="assets/plugins/datatables/jszip.min.js"></script>
<script src="assets/plugins/datatables/pdfmake.min.js"></script>
<script src="assets/plugins/datatables/vfs_fonts.js"></script>
<script src="assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="assets/plugins/datatables/buttons.print.min.js"></script>
<script src="assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
<script src="assets/plugins/datatables/dataTables.keyTable.min.js"></script>
<script src="assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="assets/plugins/datatables/dataTables.scroller.min.js"></script>
<script src="assets/pages/datatables.init.js"></script>
<script src="assets/js/app.js"></script>
</body>
<!-- Mirrored from themesdesign.in/webadmin_1.1/layouts/blue/tables-datatable.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Feb 2019 07:51:19 GMT -->
</html>