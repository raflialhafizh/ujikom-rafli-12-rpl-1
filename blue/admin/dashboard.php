<!DOCTYPE html>
<?php
include "head.php"; 
?>
<html>

											<div class="content-page">
												<div class="content">
													<div class="">
														<div class="page-header-title">
															<h4 class="page-title">Dashboard</h4>
														</div>
													</div>
													<div class="page-content-wrapper ">
														<div class="container">
															<div class="row">
																<div class="col-sm-6 col-lg-3">
																	<div class="panel text-center">
																		<div class="panel-heading">
																			<h4 class="panel-title text-muted font-light">Data Petugas</h4>
																		</div>
																		<div class="panel-body p-t-10">
																			<h2 class="m-t-0 m-b-15">
																				<i class="mdi mdi mdi-account text-danger m-r-10"></i>
																				<b>
                                                                                    <?php
                                                                                    $query=mysqli_query($konek, "SELECT COUNT(id_petugas) FROM petugas");
                                                                                    $ambilquery=mysqli_fetch_array ($query);
                                                                                    echo $ambilquery[0]; 
                                                                                    ?>

                                                                                 </b>
																			</h2>
																			<p class="text-muted m-b-0 m-t-20">
																				<b>_______________________</b> 
																			</p>
																		</div>
																	</div>
																</div>
																<div class="col-sm-6 col-lg-3">
																	<div class="panel text-center">
																		<div class="panel-heading">
																			<h4 class="panel-title text-muted font-light">Data Pegawai</h4>
																		</div>
																		<div class="panel-body p-t-10">
																			<h2 class="m-t-0 m-b-15">
																				<i class="mdi mdi mdi-account-multiple text-primary m-r-10"></i>
																				<b> <?php
                                                                                    $query=mysqli_query($konek, "SELECT COUNT(id_pegawai) FROM pegawai");
                                                                                    $ambilquery=mysqli_fetch_array ($query);
                                                                                    echo $ambilquery[0]; 
                                                                                    ?></b>
																			</h2>
																			<p class="text-muted m-b-0 m-t-20">
																				<b>_______________________</b>
																			</p>
																		</div>
																	</div>
																</div>
																<div class="col-sm-6 col-lg-3">
																	<div class="panel text-center">
																		<div class="panel-heading">
																			<h4 class="panel-title text-muted font-light">Data invantaris</h4>
																		</div>
																		<div class="panel-body p-t-10">
																			<h2 class="m-t-0 m-b-15">
																				<i class="mdi mdi-folder-multiple text-primary m-r-10"></i>
																				<b> <?php
                                                                                    $query=mysqli_query($konek, "SELECT COUNT(id_inventaris) FROM invantaris");
                                                                                    $ambilquery=mysqli_fetch_array ($query);
                                                                                    echo $ambilquery[0]; 
                                                                                    ?></b>
																			</h2>
																			<p class="text-muted m-b-0 m-t-20">
																				<b>_______________________</b>
																			</p>
																		</div>
																	</div>
																</div>
																<div class="col-sm-6 col-lg-3">
																	<div class="panel text-center">
																		<div class="panel-heading">
																			<h4 class="panel-title text-muted font-light">Data Peminjaman</h4>
																		</div>
																		<div class="panel-body p-t-10">
																			<h2 class="m-t-0 m-b-15">
																				<i class="mdi mdi-folder-upload text-danger m-r-10"></i>
																				<b><?php
                                                                                    $query=mysqli_query($konek, "SELECT COUNT(id_peminjaman) FROM peminjaman");
                                                                                    $ambilquery=mysqli_fetch_array ($query);
                                                                                    echo $ambilquery[0]; 
                                                                                    ?></b>
																			</h2>
																			<p class="text-muted m-b-0 m-t-20">
																				<b>_______________________</b> 
																			</p>
																		</div>
																	</div>
																</div>
															</div>
															
																							</tbody>
																						</table>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<footer class="footer"> © 2016 WebAdmin - All Rights Reserved. </footer>
											</div>
										</div>
										<script src="assets/js/jquery.min.js"></script>
										<script src="assets/js/bootstrap.min.js"></script>
										<script src="assets/js/modernizr.min.js"></script>
										<script src="assets/js/detect.js"></script>
										<script src="assets/js/fastclick.js"></script>
										<script src="assets/js/jquery.slimscroll.js"></script>
										<script src="assets/js/jquery.blockUI.js"></script>
										<script src="assets/js/waves.js"></script>
										<script src="assets/js/wow.min.js"></script>
										<script src="assets/js/jquery.nicescroll.js"></script>
										<script src="assets/js/jquery.scrollTo.min.js"></script>
										<script src="assets/plugins/morris/morris.min.js"></script>
										<script src="assets/plugins/raphael/raphael-min.js"></script>
										<script src="assets/pages/dashborad.js"></script>
										<script src="assets/js/app.js"></script>
									</body>
									<!-- Mirrored from themesdesign.in/webadmin_1.1/layouts/blue/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Feb 2019 07:50:40 GMT -->
								</html>