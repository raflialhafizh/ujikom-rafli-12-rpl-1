<?php
session_start();
include 'konek.php';
if(!isset($_SESSION['username'])){
	echo "<script type=text/javascript>
	alert('Anda Belum Login');
	window.location='../index.php';</script>";
}
?>
<head>
<meta charset="utf-8"/>
<title>INVENTARIS</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<meta content="Admin Dashboard" name="description"/>
<meta content="ThemeDesign" name="author"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<link rel="shortcut icon" href="assets/images/favicon.ico">
<link href="assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="assets/css/icons.css" rel="stylesheet" type="text/css">
<link href="assets/css/style.css" rel="stylesheet" type="text/css">
</head>
<body class="fixed-left">
<div id="wrapper">
	<div class="topbar">
		<div class="topbar-left">
			<div class="text-center">
				<a href="index.html" class="logo"><span>Inventaris</span>Admin</a><a href="" class="logo-sm"><span>W</span></a>
			</div>
		</div>
		<div class="navbar navbar-default" role="navigation">
			<div class="container">
				<div class="">
					<div class="pull-left">
						<button type="button" class="button-menu-mobile open-left waves-effect waves-light"><i class="ion-navicon"></i></button><span class="clearfix"></span>
					</div>
					<form class="navbar-form pull-left" role="search">
						<div class="form-group">
							<input type="text" class="form-control search-bar" placeholder="Search..."></div>
						<button type="submit" class="btn btn-search"><i class="fa fa-search"></i></button>
					</form>
					<ul class="nav navbar-nav navbar-right pull-right">
						<li class="hidden-xs">
							<a href="#" id="btn-fullscreen" class="waves-effect waves-light notification-icon-box"><i class="mdi mdi-fullscreen"></i></a>
						</li>
						<?php
						$use=$_SESSION['username'];
						$fo=mysqli_query($konek,"SELECT nama_petugas from petugas where username='$use'");
						$f=mysqli_fetch_array($fo);
						?>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle profile waves-effect waves-light" data-toggle="dropdown" aria-expanded="true"><span class="profile-username"><?php echo $f['nama_petugas']; ?> <br/><small>Admin</small></span></a>
							<ul class="dropdown-menu">
								
								<li class="divider"></li>
								<li>
									<a href="logout.php">Logout</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="left side-menu">
		<div class="sidebar-inner slimscrollleft">
			<div class="user-details">
				<div class="text-center">
				</div>
				<div class="user-info">
					<div class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><?php echo $f['nama_petugas']; ?></a>
						<ul class="dropdown-menu">
							
							<li class="divider"></li>
							<li>
								<a href="logout.php">Logout</a>
							</li>
						</ul>
					</div>
					<p class="text-muted m-0">
						<i class="fa fa-dot-circle-o text-success"></i> Online
					</p>
				</div>
			</div>
			<div id="sidebar-menu">
				<ul>
					<li>
						<a href="dashboard.php" class="waves-effect"><i class="mdi mdi-home"></i><span> Dashboard <span class="badge badge-primary pull-right">1</span></span></a>
					</li>
					<li class="has_sub">
						<a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-album"></i><span>Inventaris</span><span class="pull-right"><i class="mdi mdi-plus"></i></span></a>
						<ul class="list-unstyled">
							<li>
								<a href="barangmasuk.php">Data Barang</a>
							</li>
							<li>
								<a href="datajenis.php">Data Jenis</a>
							</li>  	
							<li>
								<a href="dataruang.php">Data Ruang</a>
							</li>  	
						</ul>
					</li>
					<li class="has_sub">
						<a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-opacity"></i><span>Pegawai</span><span class="pull-right"><i class="mdi mdi-plus"></i></span></a>
						<ul class="list-unstyled">
							<li>
								<a href="datapegawai.php">Data Pegawai</a>
							</li>
						</ul>
					</li>
					<li class="has_sub">
						<a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-layers"></i><span>Petugas</span><span class="pull-right"><i class="mdi mdi-plus"></i></span></a>
						<ul class="list-unstyled">
							<li>
								<a href="datapetugas.php">Data Petugas</a>
							</li>
						</ul>
					</li>
					<li class="has_sub">
						<a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-map"></i><span>Transaksi</span><span class="pull-right"><i class="mdi mdi-plus"></i></span></a>
						<ul class="list-unstyled">
							<li>
								<a href="peminjaman.php">Peminjaman</a>
							</li>
							<li>
								<a href="datadipinjam.php">Data Peminjaman</a>
							</li>
							
						</ul>
					</li>
					<li class="has_sub">
						<a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-layers"></i><span>Laporan</span><span class="pull-right"><i class="mdi mdi-plus"></i></span></a>
						<ul class="list-unstyled">
							<li>
								<a href="general_laporan.php">Generate Laporan</a>
							</li>
						</ul>
					</li>
							<li class="has_sub">
						<a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-layers"></i><span>Back up</span><span class="pull-right"><i class="mdi mdi-plus"></i></span></a>
						<ul class="list-unstyled">
							<li>
								<a href="backupdb.php">Back Up Database</a>
							</li>
		
						</ul>
					</li>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>