<?php
include "head.php";
?>
<!DOCTYPE html><html>
	<div class="content-page">
		<div class="content">
			<div class="">
				<div class="page-header-title">
				</div>
			</div>
			<div class="page-content-wrapper ">
            	<div class="container">
			    	<div class="row">
						<div class="col-md-0">
                        <div class="table-responsive">
							<div class="panel panel-primary">
								<div class="panel-body">
								    <h4 class="m-b-30 m-t-0">Peminjaman</h4>
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<table id="datatable" class="table table-striped table-bordered">
            						<thead>
									<tr>

										<th>Nama</th>
										<th>Aksi</th>
									</tr>
									</thead>
                                    <tbody>
            <tr>
                <td>Inventaris</td>
                <td>
                    <div class="col-md-6">
                        <a href="export_inventaris.php" type="button" class="btn btn-3d btn-success">Excel</a>
                        <a href="pdf_inventaris.php" type="button" class="btn btn-3d btn-default">PDF</a>
                    </div>
                </td>
            </tr>
            
            <tr>
                <td>Data Pengembalian</td>
                <td>
                    <div class="col-md-6">
                        <a href="export_kembali.php" type="button" class="btn btn-3d btn-success">Excel</a>
                        <a href="pdf_kembali.php" type="button" class="btn btn-3d btn-default">PDF</a>
                    </div>
                </td>
            </tr>

            <tr>
                <td>Data Peminjaman</td>
                <td>
                    <div class="col-md-6">
                        <a href="export_pinjam.php" type="button" class="btn btn-3d btn-success">Excel</a>
                        <a href="pdf_pinjam.php" type="button" class="btn btn-3d btn-default">PDF</a>
                    </div>
                </td>
            </tr>

            <tr>
                <td>Semua Data</td>
                <td>
                    <div class="col-md-6">
                        <a href="export_all.php" type="button" class="btn btn-3d btn-success">Excel</a>
                        <a href="pdf_all.php" type="button" class="btn btn-3d btn-default">PDF</a>
                    </div>
                </td>
            </tr>
		</tbody>
              </table>
                                   
								</div>                            
							</div>
						</div>
					</div>
				</div>
            </div>
            
		</div>
		<footer class="footer">© 2016 WebAdmin - All Rights Reserved.</footer>
	</div>
</div>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/modernizr.min.js"></script>
<script src="assets/js/detect.js"></script>
<script src="assets/js/fastclick.js"></script>
<script src="assets/js/jquery.slimscroll.js"></script>
<script src="assets/js/jquery.blockUI.js"></script>
<script src="assets/js/waves.js"></script>
<script src="assets/js/wow.min.js"></script>
<script src="assets/js/jquery.nicescroll.js"></script>
<script src="assets/js/jquery.scrollTo.min.js"></script>
<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="assets/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="assets/plugins/datatables/buttons.bootstrap.min.js"></script>
<script src="assets/plugins/datatables/jszip.min.js"></script>
<script src="assets/plugins/datatables/pdfmake.min.js"></script>
<script src="assets/plugins/datatables/vfs_fonts.js"></script>
<script src="assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="assets/plugins/datatables/buttons.print.min.js"></script>
<script src="assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
<script src="assets/plugins/datatables/dataTables.keyTable.min.js"></script>
<script src="assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="assets/plugins/datatables/dataTables.scroller.min.js"></script>
<script src="assets/pages/datatables.init.js"></script>
<script src="assets/js/app.js"></script>
</body>
<!-- Mirrored from themesdesign.in/webadmin_1.1/layouts/blue/tables-datatable.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Feb 2019 07:51:19 GMT -->
</html>