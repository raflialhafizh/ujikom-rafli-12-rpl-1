<?php
include "head.php"; 
?>
<!DOCTYPE html><html>

	<div class="content-page">
		<div class="content">
			<div class="">
				<div class="page-header-title">
				</div>
			</div>
			<div class="page-content-wrapper ">
				<div class="container">		
					<div class="row">
						<div class="col-md-12">
							<div class="table-responsive">
							<div class="panel panel-primary">
								<div class="panel-body">
									<h4 class="m-b-30 m-t-0">Data Petugas</h4>
									<table id="datatable-buttons" class="table table-striped table-bordered">
										
									<thead>
									<tr>
										<th>No.</th>
										<th>Username</th>
										<th>status</th>
                                        <th>Nama Petugas</th>
                                        <th>Email</th>
                                        <th>Id Level</th>
										<th>Opsi</th>
									</tr>
									</thead>
									<tbody>
		 <?php
                    include "konek.php";
                    $query_mysql = mysqli_query ($konek, "SELECT * FROM petugas ptg INNER JOIN level lvl on ptg.id_level=lvl.id_level ORDER BY id_petugas DESC") or die (mysqli_error());
                    $i = 1;
                    while($data = mysqli_fetch_array($query_mysql)){
						?>
                                <tr>
                                    <td class="text-center"><?php echo $i++;?></td>
                                    <td class="text-center"><?php echo $data['username']; ?></td>
                                    <td><?php
											if($data['status'] == 'Y')
											{
												?>
											<a href="approve.php?table=petugas&id_petugas=<?php echo $data['id_petugas']; ?>&action=not-verifed"><font color="blue">
											Aktif</font>
											</a>
											<?php
											}else{
												?>
												
											<a href="approve.php?table=petugas&id_petugas=<?php echo $data['id_petugas']; ?>&action=verifed"><font color="red">
											Tidak Aktif</font>
											</a>
											<?php
											}
											?>
											</td>
										<td class="text-center"><?php echo $data['nama_petugas']; ?></td>
										<td class="text-center"><?php echo $data['email']; ?></td>
										<td class="text-center"><?php echo $data['nama_level']; ?></td>
										<td class="text-center">
                                        <a href="#" data-toggle="modal" data-target="#modal-edit<?php echo $data['id_petugas'];?>" class="btn btn-success">Edit</a>
                                     </td>
								
			<div class="modal fade" id="modal-edit<?php echo $data['id_petugas'];?>">
			  <div class="modal-dialog">
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Data Petugas</h4>
					  <div class="modal-body">
						<form method="POST" action="u_petugas.php">
						  <div class="box-body">
							<input type="hidden" name="id_petugas" value="<?php echo $data['id_petugas'] ?>" required>
							<div class="form-group">
							  <label>Username</label>
							  <input name="username" type="text" class="form-control" placeholder="" onkeypress="hurufangka(event)" value="<?php echo $data['username'] ?>" required>
							</div>
							<div class="form-group">
							  <label>Password</label>
							  <input name="password" type="text" class="form-control" placeholder="" onkeypress="hurufangka(event)" value="<?php echo $data['password'] ?>" required>
							</div>
							<div class="form-group">
							  <label>Nama Petugas</label>
							  <input name="nama_petugas" type="text" class="form-control" placeholder="" onkeypress="hurufangka(event)" value="<?php echo $data['nama_petugas'] ?>" required>
							</div>
							<div class="form-group">
							  <label>Email</label>
							  <input name="email" type="text" class="form-control" placeholder="" onkeypress="hurufangka(event)" value="<?php echo $data['email'] ?>" required>
							</div>
							<div class="form-group">
							  <label>Level</label>
							  <select name="id_level" class="form-control" value="<? echo $data['id_level'] ?>" required>
								  <?php
									  $select=mysqli_query($konek, "SELECT * FROM level");
									  while($show=mysqli_fetch_array($select)){
									 ?>
									 <option value="<?=$show['id_level'];?>"><?=$show['nama_level'];?>
									  </option>
									  <?php } ?>
								</select>
							</div>
							<div class="modal-footer">
							<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
							<button type="submit" class="btn btn-primary" value="update">Edit</button>
						  </div>
						</form>
					  </div>
				    </div>
				  </div>
				</div>
					<?php } ?>
								
                </tbody>
              </table>
                                    <div class="row">
											<div class="col-xs- col-sm- m-t-10">
												<div class=" text-center">
													<button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#myModal">+ Tambah Data</button>
												</div>
												<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
													<div class="modal-dialog">
														<div class="modal-content">
															<div class="modal-header">
																<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
																<h4 class="modal-title" id="myModalLabel">Input Data Petugas</h4>
															</div>
															<div class="modal-body">
                                                            <div class="col-md-12">
                                                            <div class="row">
						<div class="col-sm-12">
							<div class="panel panel-primary">
								<div class="panel-body">
									<form action="proses_simpan_petugas.php" method="post" class="form-horizontal" role="form">
										<div class="form-group">
											<label class="col-md-2 control-label">Username</label>
											<div class="col-md-10">
												<input type="text" class="form-control" name="username" placeholder="Username" onkeypress="hurufangka(event)" required></div>
                                        </div>
                                        <div class="form-group">
											<label class="col-md-2 control-label">password</label>
											<div class="col-md-10">
												<input type="text" class="form-control" name="password" placeholder="Password" onkeypress="hurufangka(event)" required></div>
											</div>
										<div class="form-group">
											<label class="col-md-2 control-label">Nama petugas</label>
											<div class="col-md-10">
												<input type="text" class="form-control" name="nama_petugas" placeholder="Nama" onkeypress="hurufangka(event)" required></div>
										</div>
										<div class="form-group">
											<label class="col-md-2 control-label">Email</label>
											<div class="col-md-10">
											<input type="text" class="form-control" name="email" placeholder="Email" onkeypress="hurufangka(event)" required></div>
									  
                                        </div>
																				<div class="form-group">
											<label class="col-md-2 control-label">Id Level</label>
											<div class="col-md-10">
											<select name="id_level" class="form-control" required>
													<?php
														$select=mysqli_query($konek, "SELECT * FROM level");
														while($show=mysqli_fetch_array($select)){
														?>
														<option value="<?=$show['id_level'];?>"><?=$show['nama_level'];?>
														</option>
														<?php } ?>
													</select></div>
										</div>
										<div class="form-group m-b-0">
											<div class="col-sm-offset-8 col-sm-9">
												<button type="submit" name="send" class="btn btn-info waves-effect waves-light">Simpan</button>
											</div>
                                        </div>
									</form>
								</div>
							</div>
						</div>
					</div>
					
                                                        </div>
															<div class="modal-footer">
														
															</div>
														</div>
													</div>
												</div>
											</div>
										
								</div>
							</div>
						</div>
					</div>
				</div>
            </div>
            
		</div>
		<footer class="footer">© 2016 WebAdmin - All Rights Reserved.</footer>
	</div>
</div>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/modernizr.min.js"></script>
<script src="assets/js/detect.js"></script>
<script src="assets/js/fastclick.js"></script>
<script src="assets/js/jquery.slimscroll.js"></script>
<script src="assets/js/jquery.blockUI.js"></script>
<script src="assets/js/waves.js"></script>
<script src="assets/js/wow.min.js"></script>
<script src="assets/js/jquery.nicescroll.js"></script>
<script src="assets/js/jquery.scrollTo.min.js"></script>
<script type="text/javascript" src="assets/js/input_detect.js"></script>
<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="assets/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="assets/plugins/datatables/buttons.bootstrap.min.js"></script>
<script src="assets/plugins/datatables/jszip.min.js"></script>
<script src="assets/plugins/datatables/pdfmake.min.js"></script>
<script src="assets/plugins/datatables/vfs_fonts.js"></script>
<script src="assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="assets/plugins/datatables/buttons.print.min.js"></script>
<script src="assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
<script src="assets/plugins/datatables/dataTables.keyTable.min.js"></script>
<script src="assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="assets/plugins/datatables/dataTables.scroller.min.js"></script>
<script src="assets/pages/datatables.init.js"></script>
<script src="assets/js/app.js"></script>
</body>
<!-- Mirrored from themesdesign.in/webadmin_1.1/layouts/blue/tables-datatable.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Feb 2019 07:51:19 GMT -->
</html>