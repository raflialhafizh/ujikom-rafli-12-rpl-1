<!DOCTYPE html>
<html>
<head>
  <title>Inventaris SMK</title>
</head>
<body>
  <style type="text/css">
  body{
    font-family: sans-serif;
  }
  table{
    margin: 20px auto;
    border-collapse: collapse;
  }
  table th,
  table td{
    border: 1px solid #3c3c3c;
    padding: 3px 8px;

  }
  a{
    background: blue;
    color: #fff;
    padding: 8px 10px;
    text-decoration: none;
    border-radius: 2px;
  }
  </style>

  <?php
  header("Content-type: application/vnd-ms-excel");
  header("Content-Disposition: attachment; filename=Data Inventaris.xls");
  ?>

  <center>
    <h1>SMK NEGERI 1 CIOMAS</h1><br>
    <h3>Telp : (0251)8631261. JL. Raya Laladon Ds.Laladon, Kec.Ciomas Kab.Bogor Kode Pos. 16610</h3></br>
    <h3>Email : smkn1_ciomas@yahoo.co.id, Website : www.smkn1ciomas.sch.id</h3>
  </center>

  <table border="1">
   <thead>
    <tr>
      <th>No</th>
      <th>Nama Barang</th>
      <th>Kondisi</th> 
      <th>Keterangan</th>
      <th>Jumlah</th>
      <th>Nama Jenis</th>
      <th>Tanggal Register</th>
      <th>Nama Ruang</th>
      <th>Kode Inventaris</th>
      <th>Nama Petugas</th> 
    </tr>
  </thead>
 
<tbody>
<?php
    include 'konek.php';
      $no=1;
      $query = mysqli_query ($konek, "SELECT * FROM invantaris INNER JOIN jenis on invantaris.id_jenis = jenis.id_jenis 
				INNER JOIN ruang on invantaris.id_ruang = ruang.id_ruang INNER JOIN petugas on invantaris.id_petugas = petugas.id_petugas 
				ORDER BY id_inventaris DESC") or die (mysql_error());
    while ($data = mysqli_fetch_array($query))
  {
?>

  <tr>
    <td><?php echo $no++; ?></td>
    <td><?php echo $data['nama']; ?></td>
    <td><?php echo $data['kondisi']; ?></td> 
    <td><?php echo $data['keterangan']; ?></td>
    <td><?php echo $data['jumlah']; ?></td>
    <td><?php echo $data['nama_jenis']; ?></td>
    <td><?php echo $data['tgl_register']; ?></td>
    <td><?php echo $data['nama_ruang']; ?></td>
    <td><?php echo $data['kode_inventaris']; ?></td>
    <td><?php echo $data['nama_petugas']; ?></td>        
  </tr>
<?php
  }
?>
</tbody>
</table>                              
</body>
</html>