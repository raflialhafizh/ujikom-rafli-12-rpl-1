<?php
include "head.php"; 
?>
<!DOCTYPE html><html>

	<div class="content-page">
		<div class="content">
			<div class="">
				<div class="page-header-title">
				</div>
			</div>
			<div class="page-content-wrapper ">
				<div class="container">		
					<div class="row">
						<div class="col-md-12">
							<div class="table-responsive">
							<div class="panel panel-primary">
								<div class="panel-body">
									<h4 class="m-b-30 m-t-0">Data Ruangan</h4>
									<table id="datatable-buttons" class="table table-striped table-bordered">
										
									<thead>
									<tr>
										<th>No.</th>
										<th>Nama Ruangan</th>
										<th>Kode Ruangan</th>
										<th>keterangan</th>
                                        <th>Opsi</th>
									</tr>
									</thead>
									<tbody>
		 <?php
                    include "konek.php";
                    $query_mysql = mysqli_query ($konek, "SELECT * FROM ruang ORDER BY id_ruang") or die (mysqli_error());
                    $i = 1;
                    while($data = mysqli_fetch_array($query_mysql)){
						?>
                                <tr>
                                    <td class="text-center"><?php echo $i++;?></td>
                                    <td class="text-center"><?php echo $data['nama_ruang']; ?></td>
                                    <td class="text-center"><?php echo $data['kode_ruang']; ?></td>
									<td class="text-center"><?php echo $data['keterangan']; ?></td>
									<td class="text-center">
                                   <a href="#" data-toggle="modal" data-target="#modal-edit<?php echo $data['id_ruang'];?>" class="btn btn-success">Edit</a>
                                     </td>
								
			<div class="modal fade" id="modal-edit<?php echo $data['id_ruang'];?>">
			  <div class="modal-dialog">
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Data Ruang</h4>
					  <div class="modal-body">
						<form method="POST" action="u_ruang.php">
						  <div class="box-body">
							<input type="hidden" name="id_ruang" value="<?php echo $data['id_ruang'] ?>" required>
							<div class="form-group">
							  <label>Nama Ruang</label>
							  <input name="nama_ruang" type="text" class="form-control" placeholder="" onkeypress="hurufangka(event)" value="<?php echo $data['nama_ruang'] ?>" required>
							</div>
							<div class="form-group">
							  <label>Kode Ruang</label>
							  <input name="kode_ruang" type="text" class="form-control" placeholder="" readonly="" value="<?php echo $data['kode_ruang'] ?>" required>
							</div>
							<div class="form-group">
							  <label>keterangan</label>
							  <input name="keterangan" type="text" class="form-control" placeholder="" onkeypress="hurufangka(event)" value="<?php echo $data['keterangan'] ?>" required>
							</div>
							<div class="modal-footer">
							<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
							<button type="submit" class="btn btn-primary" value="update">Edit</button>
						  </div>
						</form>
					  </div>
				    </div>
				  </div>
				</div>
					<?php } ?>
								
                </tbody>
              </table>
                                    <div class="row">
											<div class="col-xs- col-sm- m-t-10">
												<div class=" text-center">
													<button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#myModal">+ Tambah Data</button>
												</div>
												<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
													<div class="modal-dialog">
														<div class="modal-content">
															<div class="modal-header">
																<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
																<h4 class="modal-title" id="myModalLabel">Input Data Ruang</h4>
															</div>
															<div class="modal-body">
                                                            <div class="col-md-12">
                                                            <div class="row">
						<div class="col-sm-12">
							<div class="panel panel-primary">
								<div class="panel-body">
									<form action="proses_simpan_ruang.php" method="post" class="form-horizontal" role="form">
										<div class="form-group">
											<label class="col-md-2 control-label">Nama Ruangan</label>
											<div class="col-md-10">
												<input type="text" class="form-control" name="nama_ruang" placeholder="Nama Ruang" onkeypress="hurufangka(event)" required></div>
                          </div>
													<?php
							  $konek = mysqli_connect('localhost','root','','inventaris');

							  $cari_kd = mysqli_query($konek, "select max(kode_ruang) as kode from ruang");

							  $tm_cari = mysqli_fetch_array($cari_kd);
							  $kode=substr($tm_cari['kode'], 3,4);

							  $tambah =$kode+1;

							  if ($tambah<10){
							  		$kode_ruang="RNG000".$tambah;
							  }else{
							  		$kode_ruang="RNG00".$tambah;
							  }
							  ?>

										<div class="form-group">
											<label class="col-md-2 control-label">Kode Ruang</label>
											<div class="col-md-10">
											<input type="text" class="form-control" name="kode_ruang" readonly="" value="<?php echo $kode_ruang ?>" required></div>
										</div>
										<div class="form-group">
											<label class="col-md-2 control-label">Keterangan</label>
											<div class="col-md-10">
												<input type="text" class="form-control" name="keterangan" placeholder="Keterangan" onkeypress="hurufangka(event)" required></div>
										</div>
										
										<div class="form-group m-b-0">
											<div class="col-sm-offset-8 col-sm-9">
												<button type="submit" name="send" class="btn btn-info waves-effect waves-light">Simpan</button>
											</div>
                                        </div>
									</form>
								</div>
							</div>
						</div>
					</div>
					
                                                        </div>
															<div class="modal-footer">
														
															</div>
														</div>
													</div>
												</div>
											</div>
										
								</div>
							</div>
						</div>
					</div>
				</div>
            </div>
            
		</div>
		<footer class="footer">© 2016 WebAdmin - All Rights Reserved.</footer>
	</div>
</div>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/modernizr.min.js"></script>
<script src="assets/js/detect.js"></script>
<script src="assets/js/fastclick.js"></script>
<script src="assets/js/jquery.slimscroll.js"></script>
<script src="assets/js/jquery.blockUI.js"></script>
<script src="assets/js/waves.js"></script>
<script src="assets/js/wow.min.js"></script>
<script src="assets/js/jquery.nicescroll.js"></script>
<script src="assets/js/jquery.scrollTo.min.js"></script>
<script type="text/javascript" src="assets/js/input_detect.js"></script>
<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="assets/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="assets/plugins/datatables/buttons.bootstrap.min.js"></script>
<script src="assets/plugins/datatables/jszip.min.js"></script>
<script src="assets/plugins/datatables/pdfmake.min.js"></script>
<script src="assets/plugins/datatables/vfs_fonts.js"></script>
<script src="assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="assets/plugins/datatables/buttons.print.min.js"></script>
<script src="assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
<script src="assets/plugins/datatables/dataTables.keyTable.min.js"></script>
<script src="assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="assets/plugins/datatables/dataTables.scroller.min.js"></script>
<script src="assets/pages/datatables.init.js"></script>
<script src="assets/js/app.js"></script>
</body>
<!-- Mirrored from themesdesign.in/webadmin_1.1/layouts/blue/tables-datatable.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Feb 2019 07:51:19 GMT -->
</html>