<?php
include "head.php";
?>
<!DOCTYPE html><html>
	<div class="content-page">
		<div class="content">
			<div class="">
				<div class="page-header-title">
				</div>
			</div>
			<div class="page-content-wrapper ">
            	<div class="container">
			    	<div class="row">
						<div class="col-md-0">
                        <div class="table-responsive">
							<div class="panel panel-primary">
								<div class="panel-body">
								    <h4 class="m-b-30 m-t-0">Peminjaman</h4>
									<form action="proses_tambah_peminjam.php" method="post">
                                        <?php
                                        include 'konek.php';
                                        $use=$_SESSION['username'];
                                        $result = mysqli_query($konek,"select * from pegawai where username='$use'");
                                        $jsArray = "var id_pegawai = new Array();\n";
                                        ?>
                                    <div class="form-group">
                                      <label for="recipient-name" class="control-label">Id/Nama</label>
                                      <input type="text"  class="form-control" name="id_pegawai" value="<?php while($row = mysqli_fetch_array($result)){echo"$row[0]/$row[1]";
                                      $jsArray = "id_pegawai['".$row['id_pegawai']. "'] = {satu:'" . addslashes($row['id_pegawai']) . "'};\n";
                                    }?>" readonly=""/>
                                    </div>  
									<div class="form-group">
                                      <label for="recipient-name" class="control-label">Tanggal Peminjaman</label>
                                      <input type="date" id="date" class="form-control" name="tgl_pinjam" required value="<?php echo date("Y-m-d"); ?>" class="form-control"  readonly>
                                    </div>  
                                    <div class="form-group">
                                      <label for="recipient-name" class="control-label">Tanggal Pengembalian</label>
                                      <input type="date" id="date" class="form-control" name="tgl_kembali" value="<?php echo date("Y-m-d"); ?>" required/>
                                    </div>

                              </div>
                                <div class="modal-footer">
                                  <button type="submit" class="btn btn-danger waves-effect waves-light">Simpan</button>
                                </div>	
                </tbody>
              </table>
                                    
					
                                                        </div>
															<div class="modal-footer">
														
															</div>
														</div>
													</div>
												</div>
											</div>
										
								</div>                            
							</div>
						</div>
					</div>
				</div>
            </div>
            
		</div>
		<footer class="footer">© 2016 WebAdmin - All Rights Reserved.</footer>
	</div>
</div>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/modernizr.min.js"></script>
<script src="assets/js/detect.js"></script>
<script src="assets/js/fastclick.js"></script>
<script src="assets/js/jquery.slimscroll.js"></script>
<script src="assets/js/jquery.blockUI.js"></script>
<script src="assets/js/waves.js"></script>
<script src="assets/js/wow.min.js"></script>
<script src="assets/js/jquery.nicescroll.js"></script>
<script src="assets/js/jquery.scrollTo.min.js"></script>
<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="assets/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="assets/plugins/datatables/buttons.bootstrap.min.js"></script>
<script src="assets/plugins/datatables/jszip.min.js"></script>
<script src="assets/plugins/datatables/pdfmake.min.js"></script>
<script src="assets/plugins/datatables/vfs_fonts.js"></script>
<script src="assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="assets/plugins/datatables/buttons.print.min.js"></script>
<script src="assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
<script src="assets/plugins/datatables/dataTables.keyTable.min.js"></script>
<script src="assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="assets/plugins/datatables/dataTables.scroller.min.js"></script>
<script src="assets/pages/datatables.init.js"></script>
<script src="assets/js/app.js"></script>
</body>
<!-- Mirrored from themesdesign.in/webadmin_1.1/layouts/blue/tables-datatable.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Feb 2019 07:51:19 GMT -->
</html>