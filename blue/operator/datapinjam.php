<?php
include "head.php"; 
?>
<!DOCTYPE html><html>

	<div class="content-page">
		<div class="content">
			<div class="">
				<div class="page-header-title">
				</div>
			</div>
			<div class="page-content-wrapper ">
				<div class="container">		
					<div class="row">
						<div class="col-md-12">
							<div class="table-responsive">
							<div class="panel panel-primary">
								<div class="panel-body">
									<h4 class="m-b-30 m-t-0">Data Peminjaman</h4>
									<table id="datatable-buttons" class="table table-striped table-bordered">
										
									<thead>
									<tr>
										<th>No.</th>
										<th>Nama Peminjam</th>
										<th>Tanggal Pinjam</th>
									<th>Tanggal Kembali</th>
									<th>Status Peminjaman</th>
										<th>Id Peminjam</th>
														<th>Detail</th>
									</tr>
									</thead>
									<tbody>
									<?php
										include "konek.php";
										$query_mysql = mysqli_query ($konek, "SELECT * FROM peminjaman INNER JOIN pegawai on peminjaman.id_pegawai = pegawai.id_pegawai 
										ORDER BY id_peminjaman DESC") or die (mysql_error());
										$i = 1;
										while($data = mysqli_fetch_array($query_mysql)){
									?>
                                <tr>
                                    <td class="text-center"><?php echo $i++;?></td>
                                    <td class="text-center"><?php echo $data['nama_pegawai']; ?></td>
                                    <td class="text-center"><?php echo $data['tgl_pinjam']; ?></td>
									<td class="text-center"><?php echo $data['tgl_kembali']; ?></td>
                                    <td class="text-center"><?php echo $data['status_peminjaman']; ?></td>
									<td class="text-center"><?php echo $data['id_peminjaman']; ?></td>
                                    <td class="text-center">
									<a href="detail_pinjam.php?id_peminjaman=<?php echo $data['id_peminjaman'];?>"
										 type="button" class="btn btn-primary">View</button>
                                    </td>
								</tr>
								
								
								
										<?php } ?>
										</tbody>
								</table>
                                                        </div>
															<div class="modal-footer">
														
															</div>
														</div>
													</div>
												</div>
											</div>
										
								</div>
							</div>
						</div>
					</div> 	
            
		</div>
		<footer class="footer">© 2016 WebAdmin - All Rights Reserved.</footer>
	</div>
</div>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/modernizr.min.js"></script>
<script src="assets/js/detect.js"></script>
<script src="assets/js/fastclick.js"></script>
<script src="assets/js/jquery.slimscroll.js"></script>
<script src="assets/js/jquery.blockUI.js"></script>
<script src="assets/js/waves.js"></script>
<script src="assets/js/wow.min.js"></script>
<script src="assets/js/jquery.nicescroll.js"></script>
<script src="assets/js/jquery.scrollTo.min.js"></script>
<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="assets/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="assets/plugins/datatables/buttons.bootstrap.min.js"></script>
<script src="assets/plugins/datatables/jszip.min.js"></script>
<script src="assets/plugins/datatables/pdfmake.min.js"></script>
<script src="assets/plugins/datatables/vfs_fonts.js"></script>
<script src="assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="assets/plugins/datatables/buttons.print.min.js"></script>
<script src="assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
<script src="assets/plugins/datatables/dataTables.keyTable.min.js"></script>
<script src="assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="assets/plugins/datatables/dataTables.scroller.min.js"></script>
<script src="assets/pages/datatables.init.js"></script>
<script src="assets/js/app.js"></script>
</body>
<!-- Mirrored from themesdesign.in/webadmin_1.1/layouts/blue/tables-datatable.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Feb 2019 07:51:19 GMT -->
</html>